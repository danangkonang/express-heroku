FROM node:lts-alpine3.12

ENV NODE_ENV=production

WORKDIR /app
 
COPY dist .
 
EXPOSE 9000

CMD [ "node", "main.js" ]
