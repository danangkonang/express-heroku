const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
process.env.TZ = 'Asia/Jakarta';
const port = process.env.PORT || 9000;
app.use(cors({
  origin: '*',
  methods: ['GET', 'POST', 'DELETE', 'PUT'],
  allowedHeaders: ['Content-Type', 'Authorization', 'Accept'],
}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.status(200).json({
    message: 'pong',
  });
});
app.use(require('./router'));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
