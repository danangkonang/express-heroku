/* eslint-disable no-unused-vars */

const obats = [
  {
    obat_name: 'paracetamol',
    price: 10000,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    obat_name: 'amoxcilin',
    price: 20000,
    created_at: new Date(),
    updated_at: new Date(),
  },
];

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('obats', obats),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('obats', null, {}),
};
