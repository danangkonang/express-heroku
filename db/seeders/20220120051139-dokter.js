/* eslint-disable no-unused-vars */

const dokters = [
  {
    user_id: 2,
    dokter_name: 'dr.ahmad',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    user_id: 3,
    dokter_name: 'dr.budi',
    created_at: new Date(),
    updated_at: new Date(),
  },
];

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('dokters', dokters),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('dokters', null, {}),
};
