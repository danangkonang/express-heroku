/* eslint-disable no-unused-vars */

const reseps = [
  {
    rekam_medis_id: 1,
    obat_id: 1,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    rekam_medis_id: 1,
    obat_id: 2,
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    rekam_medis_id: 2,
    obat_id: 2,
    created_at: new Date(),
    updated_at: new Date(),
  },
];

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('reseps', reseps),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('reseps', null, {}),
};
