/* eslint-disable no-unused-vars */
const visitors = [
  {
    pasien_id: 1,
    dokter_id: 1,
    status: 'waiting',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    pasien_id: 2,
    dokter_id: 1,
    status: 'waiting',
    created_at: new Date(),
    updated_at: new Date(),
  },
];

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('visitors', visitors),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('visitors', null, {}),
};
