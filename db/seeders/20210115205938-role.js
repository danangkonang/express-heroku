/* eslint-disable no-unused-vars */

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('roles', [
    {
      role_name: 'super_user',
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      role_name: 'dokter',
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      role_name: 'kasir',
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      role_name: 'resepsionis',
      created_at: new Date(),
      updated_at: new Date(),
    },
  ]),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('roles', null, {}),
};
